import { outputAst } from '@angular/compiler';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {



  constructor() { }
  
  mensaje: string = 'Primer Mensaje del hijo al padre';
  mensaje2: string = 'Segundo Mensaje del hijo';
  mensaje3: string = 'Tercer mensaje desde el hijo';
  mensaje4: string = 'cuarto mensaje desde el hijo';
  mensaje5: string = 'quinto mensaje desde el hijo'

  @Output() Mensaje1 = new EventEmitter<string>();
  @Output() Mensaje2 = new EventEmitter<string>();
  @Output() Mensaje3 = new EventEmitter<string>();
  @Output() Mensaje4 = new EventEmitter<string>();
  @Output() Mensaje5 = new EventEmitter<string>();

  ngOnInit(): void {
    this.Mensaje1.emit(this.mensaje);
    this.Mensaje2.emit(this.mensaje2)
    this.Mensaje3.emit(this.mensaje3)
    this.Mensaje4.emit(this.mensaje4)
    this.Mensaje5.emit(this.mensaje5)
  }

}
